# **Atomik: A Dubstep Calculator**
-----
This is a project to learn layouts in HTML/CSS and add functionality using javascript

# **Development Libraries and Tools**
-----
* Node.js       - Full stack web framework
* Nodemon       - Utility that monitors any changes to files in project and automatically restart your server.
* Express       - Routing module that handles the serving of files to different routes.
* HTML          - Hypertext Markup Language
* CSS           - Cascading Style Sheets

## **Installation Instructions**
1. Clone the project
2. Run 
    $ npm install
3. Run 
    $ nodemon
4. Verify server runs by connecting to localhost:3000 

## **Purpose and Dependency Installation Instructions (from scratch)**
### **Node.JS**
This is a javascript runtime that is the engine for the project.

[NodeJS Homepage](https://nodejs.org/en/)
#### First time installation Instructions on PC/MAC:
    $ Download installer from https://nodejs.org/en/download/

#### Use Instructions:
    $ npm init
Creates a new package.json that specifies details about the project and dependency versions
    $ node <js entry>
Runs the entry javascript file of the project

### **Express**
Used as a routing framework that will serve different html files depending on the route.

[Express Homepage](http://expressjs.com/)
#### First time installation Instructions using npm:
    $ npm install -g express
This installs express into your node directory on your PC

### **Nodemon**
This is a utility that monitors different project files. Whenever these project files are modified the node.js server is restarted.

[Nodemon Homepage](http://nodemon.io/)
#### First time installation Instructions using npm:
    $ npm install -g nodemon
This installs nodemon into your node directory on your PC

#### Use Instructions:
    $ nodemon
Starts nodemon that will monitor the index.js by default. This will only restart your server when any js file is modified. nodemon can also be run by specifying the entry js file.
    $ node -e 'js,html,css'
Runs default index.js file and monitors all js,html,css files.

# **Git Workflow**
-----
When working on a part of the project make sure to:

- ALWAYS create an ISSUE with the appropriate tag and add any relevant people as watchers. 
- ALWAYS work on a different local branch, DO NOT push any changes directly to master. Branch naming convention <handle>/<objective> ex. fiveletter/login_screen
- ALWAYS create pull requests (PR) with an ISSUE attached to the PR.
- ALWAYS squash commits before merging to MASTER. This will allow us to manage commits better on MASTER
- ALWAYS rebase from latest MASTER before merging to MASTER.
- ALWAYS get approval from at least one person before merging into MASTER

Following these guidelines will allow us to catch merge conflicts and reduce the amount of work we do when trying to resolve errors within MASTER.

# **Requirements for Atomik**
-----
## Calculator
1. Users have the ability to input 0-9 numbers into the calculator.
2. Users can do basic arthmetic operations, - + * /.
3. Users can see their previous operations

## Electronic Synthesizer
1. Each number corresponds to a different sound that will be played on the device audio output
2. User will be able to toggle a back track that will mix with any sound played by the user.