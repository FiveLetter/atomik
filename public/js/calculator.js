const ALLOWED_OPERATIONS = "-+÷×"
var left_operator = 0;
var right_operator = 0;
var operation_queued = "";

var history_l = ["Empty!"]

function assign_operator(operator, value) {
    new_value = 0;
    if (typeof(value) === "string") {
        if (operator === "left") {}
        new_value = parseInt(value);
    } else if (typeof(value) === "number") {
        new_value = value;
    }

    if (operator === "left") {
        left_operator = new_value;
    } else if (operator === "right") {
        right_operator = new_value;
    }
}

function numClick(btn) {
    console.log("clicked on input button");
    var input = btn.value;
    var current_value = document.getElementById("calculator_display").innerHTML;
    if (current_value === "0") {
        current_value = input;
    } else {
        current_value += input;
    }

    if (operation_queued === "") {
        console.log("Operation not queued, assigning left operator");
        assign_operator("left", current_value);
    } else 
{        console.log("Operation queued, assigning right operator");
        assign_operator("right", current_value);
    }
    update_display(current_value);
    console.log("LEFT OPERATOR = " + left_operator)
    console.log("RIGHT OPERATOR = " + right_operator)
}

function opClick(btn) {
    console.log("clicked on a operation");
    var operation = btn.value;
    if ( -1 ===ALLOWED_OPERATIONS.indexOf(operation)){
        alert("INVALID OPERATION!");
    } else {
        // There is already an operation queued, Clear the queue
        if (operation_queued != "") {
            console.log("There was an operation!!! EXCUTE GO GO GO!!!");
            execute_operation(operation_queued);
        } 
        // There is no operation queued, get the right operation
        operation_queued = operation;
        update_display(0);
    }
}

function equalClick() {
    if (operation_queued != "") {
        execute_operation(operation_queued);
    }
}

function execute_operation(op) {
    console.log("Executing operation " + op);
    var old_left_operator = left_operator;
    switch(op) {
        case "-":
            left_operator -= right_operator
            break;
        case "+":
            left_operator += right_operator
            break;
        case "÷":
            left_operator /= right_operator
            break;
        case "×":
            left_operator *= right_operator
            break;
    }

    console.log("Calculated value = " + left_operator);
    update_history(old_left_operator, right_operator, op,left_operator);
    update_display(left_operator);
    right_operator = 0;
    operation_queued = "";
}


function update_history(left_op, right_op, op, answer) {
    if (history_l[0] === "Empty!") {
        history_l[0] = left_op + " " + op + " " + right_op + " = " + answer;
    } 
    else {
        var next_index = history_l.length;
        history_l[next_index] = left_op + " " + op + " " + right_op + " = " + answer;
    }

    var new_html = "";

    for (i = 0; i < history_l.length; i++) {
        new_html += "<li>" + history_l[i] + "</li>\n";
    }
    
    document.getElementById("history").innerHTML = new_html;
}

function update_display(value) {
    document.getElementById("calculator_display").innerHTML = value; 
}

function clear_display() {
    console.log("clkicked clear");
    document.getElementById("calculator_display").innerHTML = "0";
    left_operator = 0;
    right_operator = 0;
    operation_queued = "";
}

